package com.example.viewpager2_swipescreen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.viewpager2.widget.ViewPager2
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapter = ViewPagerAdapter()
        view_pager2.adapter = adapter
        //below line can be used to change the orientation to vertical i.e. vertical swipe
//        view_pager2.orientation = ViewPager2.ORIENTATION_VERTICAL



    }
}
